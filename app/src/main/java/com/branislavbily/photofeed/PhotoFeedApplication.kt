package com.branislavbily.photofeed

import android.app.Application
import com.branislavbily.photofeed.di.addImageModule
import com.branislavbily.photofeed.di.appModule
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin

class PhotoFeedApplication : Application() {
    override fun onCreate() {
        super.onCreate()
        // Start Koin
        startKoin{
            androidLogger()
            androidContext(this@PhotoFeedApplication)
            modules(appModule + addImageModule)
        }
    }
}