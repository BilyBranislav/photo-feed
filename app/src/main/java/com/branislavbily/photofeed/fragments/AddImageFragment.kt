package com.branislavbily.photofeed.fragments

import android.app.Activity
import android.arch.lifecycle.Observer
import android.content.Intent
import android.databinding.DataBindingUtil
import android.databinding.ViewDataBinding
import android.os.Bundle
import android.provider.MediaStore
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.branislavbily.photofeed.BR
import com.branislavbily.photofeed.R
import com.branislavbily.photofeed.viewmodel.AddImageViewModel
import kotlinx.android.synthetic.main.fragment_add_image.*
import org.koin.android.viewmodel.ext.android.viewModel
import java.io.IOException

class AddImageFragment: Fragment() {

    private val addImageViewModel: AddImageViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        observeLiveData()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val binding = DataBindingUtil.inflate<ViewDataBinding>(inflater, R.layout.fragment_add_image, container, false)
        val rootView = binding.root
        binding.setVariable(BR.viewmodel, addImageViewModel)
        binding.lifecycleOwner = this
        return rootView
    }

    private fun observeLiveData() {
        addImageViewModel.startImagePickLd.observe(this, Observer {
            val intent = Intent(Intent.ACTION_PICK,
                    MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
            startActivityForResult(intent, 0)
        })
        addImageViewModel.loadingInProgressLd.observe(this, Observer {
            if (it == true) {
                progressBarCircular?.visibility = View.VISIBLE
            } else {
                progressBarCircular?.visibility = View.GONE
            }
        })
        addImageViewModel.toastLd.observe(this, Observer {
            Toast.makeText(activity, it, Toast.LENGTH_SHORT).show()
        })
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            try {
                addImageViewModel.onImageSelected(data?.data)
            } catch (e: IOException) {
                e.printStackTrace()
            }
        }
    }
}