package com.branislavbily.photofeed.fragments

import android.arch.lifecycle.Observer
import android.content.Context
import android.content.Intent
import android.databinding.DataBindingUtil
import android.databinding.ViewDataBinding
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.branislavbily.photofeed.BR
import com.branislavbily.photofeed.R
import com.branislavbily.photofeed.activities.LoginActivity
import com.branislavbily.photofeed.domain.repository.UserRepository
import com.branislavbily.photofeed.viewmodel.AccountViewModel
import com.firebase.ui.auth.AuthUI
import org.koin.android.ext.android.inject
import org.koin.android.viewmodel.ext.android.viewModel

class AccountFragment : Fragment() {

    private val accountViewModel: AccountViewModel by viewModel()
    private val userRepository: UserRepository by inject()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        observeLiveData()
    }

    private fun observeLiveData() {
        accountViewModel.logout.observe(this, Observer {
            if(it == true) {
                logout()
            }
        })
    }

    private fun logout() {
        AuthUI.getInstance()
                .signOut(activity as Context)
                .addOnCompleteListener {
                    userRepository.logOut()
                    activity?.startActivity(Intent(activity, LoginActivity::class.java))
                }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val binding = DataBindingUtil.inflate<ViewDataBinding>(inflater, R.layout.fragment_account, container, false)
        val rootView = binding.root
        binding.setVariable(BR.viewmodel, accountViewModel)
        binding.lifecycleOwner = this

        return rootView
    }


}