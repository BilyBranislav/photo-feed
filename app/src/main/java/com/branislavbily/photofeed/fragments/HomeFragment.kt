package com.branislavbily.photofeed.fragments

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.branislavbily.photofeed.R
import com.branislavbily.photofeed.adapters.ImageAdapterKotlin
import com.branislavbily.photofeed.domain.usecase.PhotoUriUseCase
import com.google.firebase.firestore.EventListener
import org.koin.android.ext.android.inject

class HomeFragment : Fragment() {

    private val photoUriUseCase: PhotoUriUseCase by inject()


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val rootView = inflater.inflate(R.layout.fragment_home, container, false)
        val recyclerView = rootView.findViewById<RecyclerView>(R.id.recyclerView)
        recyclerView.layoutManager = LinearLayoutManager(activity)
        val imageAdapter = ImageAdapterKotlin()
        recyclerView.adapter = imageAdapter
        photoUriUseCase.getPhotos(EventListener { list, _ ->
            imageAdapter.setNewData(list)
        })
        return rootView
    }

}