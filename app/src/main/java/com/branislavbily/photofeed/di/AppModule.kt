package com.branislavbily.photofeed.di

import com.branislavbily.photofeed.domain.repository.FirebaseIdsRepository
import com.branislavbily.photofeed.domain.repository.FirebaseIdsRepositoryImpl
import com.branislavbily.photofeed.domain.repository.SharedPreferencesUserRepository
import com.branislavbily.photofeed.domain.repository.UserRepository
import com.branislavbily.photofeed.domain.usecase.FirestorePhotoUriUseCase
import com.branislavbily.photofeed.domain.usecase.Image.GetPhotoDataUserCase
import com.branislavbily.photofeed.domain.usecase.Image.GetPhotoDataUserCaseImpl
import com.branislavbily.photofeed.domain.usecase.PhotoUriUseCase
import com.branislavbily.photofeed.domain.usecase.UserData.LoadUserDataUseCase
import com.branislavbily.photofeed.domain.usecase.UserData.LoadUserDataUseCaseImpl
import com.branislavbily.photofeed.domain.usecase.UserData.SaveUserDataUseCase
import com.branislavbily.photofeed.domain.usecase.UserData.SaveUserDataUseCaseImpl
import com.branislavbily.photofeed.domain.usecase.bitmap.*
import com.branislavbily.photofeed.domain.usecase.bitmap.upload.CompressBitmapToByteArrayUseCase
import com.branislavbily.photofeed.domain.usecase.bitmap.upload.CompressBitmapToByteArrayUseCaseImpl
import com.branislavbily.photofeed.viewmodel.AccountViewModel
import com.branislavbily.photofeed.viewmodel.AddImageViewModel
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.storage.FirebaseStorage
import org.koin.android.ext.koin.androidApplication
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module

val appModule = module {
    single { SharedPreferencesUserRepository(androidApplication()) as UserRepository }
    single { FirestorePhotoUriUseCase(get(), get(), get(), FirebaseFirestore.getInstance()) as PhotoUriUseCase }
}

val addImageModule = module {
    // bitmap
    single { CropBitmapUseCaseImpl() as CropBitmapUseCase }
    single { OrientationBitMapUseCaseImpl(androidApplication().contentResolver) as OrientationBitMapUseCase }
    single { RotateBitmapUseCaseImpl(get()) as RotateBitmapUseCase }
    single { DecodeBitmapUseCaseImpl(androidApplication().contentResolver, get(), get()) as DecodeBitmapUseCase }

    // upload
    single { CompressBitmapToByteArrayUseCaseImpl() as CompressBitmapToByteArrayUseCase }

    // firebase
    single { FirebaseIdsRepositoryImpl() as FirebaseIdsRepository }

    // handling user data
    single { SaveUserDataUseCaseImpl(get(), get(), FirebaseFirestore.getInstance()) as SaveUserDataUseCase }
    single { LoadUserDataUseCaseImpl(get(), get(), FirebaseFirestore.getInstance()) as LoadUserDataUseCase }

    // getting photo data
    single { GetPhotoDataUserCaseImpl(get()) as GetPhotoDataUserCase}

    viewModel { AddImageViewModel(get(), get(), get(), get(), FirebaseFirestore.getInstance(), FirebaseStorage.getInstance()) }
    viewModel { AccountViewModel(FirebaseAuth.getInstance().currentUser)}
}