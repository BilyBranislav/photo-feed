package com.branislavbily.photofeed.activities

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import com.branislavbily.photofeed.R
import com.branislavbily.photofeed.domain.repository.UserRepository
import com.branislavbily.photofeed.domain.usecase.userdata.SaveUserDataUseCase
import com.firebase.ui.auth.AuthUI
import com.firebase.ui.auth.IdpResponse
import com.google.firebase.auth.FirebaseAuth
import org.koin.android.ext.android.inject

class LoginActivity : AppCompatActivity() {

    private val userRepository: UserRepository by inject()
    private val saveUserDataUseCase : SaveUserDataUseCase by inject()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        createSignInIntent()
    }

    fun createSignInIntent() {
        // Choose authentication providers
        val providers = listOf(
                // Many more Providers can be set up
                AuthUI.IdpConfig.EmailBuilder().build())

        // Create and launch sign-in intent
        startActivityForResult(
                AuthUI.getInstance()
                        .createSignInIntentBuilder()
                        .setIsSmartLockEnabled(false)
                        .setAvailableProviders(providers)
                        .setTheme(R.style.AppTheme)                      // Set theme
                        .setTosAndPrivacyPolicyUrls(
                                "https://example.com/terms.html",
                                "https://example.com/privacy.html")
                        .build(),
                RC_SIGN_IN)
    }

    // [START auth_fui_result]
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == RC_SIGN_IN) {
            val response = IdpResponse.fromResultIntent(data)

            if (resultCode == Activity.RESULT_OK) {
                // Successfully signed in
                val user = FirebaseAuth.getInstance().currentUser

                if (user != null) {
                    userRepository.setUID(user.uid)
                    saveUserDataUseCase.saveUserData(user)
                }
            } else {
                // Sign in failed. If response is null the user canceled the
                // sign-in flow using the back button. Otherwise check
                // response.getError().getErrorCode() and handle the error.
                // ...
                Log.i(javaClass.simpleName, "Error: " + response?.error?.errorCode)
                // TODO SHOW DIALOG WITH ERROR (MOSTLY NETWORK -> Wrong password is handled by FirebaseAuthUI)
            }
        }
        // Exit LoginActivity
        finish()
    }

    companion object {
        private val RC_SIGN_IN = 123
    }
}