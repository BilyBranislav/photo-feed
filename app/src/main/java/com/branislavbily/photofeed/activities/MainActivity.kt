package com.branislavbily.photofeed.activities

import android.content.Intent
import android.os.Bundle
import android.support.design.widget.BottomNavigationView
import android.support.v7.app.AppCompatActivity
import com.branislavbily.photofeed.R
import com.branislavbily.photofeed.adapters.CustomPagerAdapter
import com.branislavbily.photofeed.domain.repository.UserRepository
import com.google.firebase.FirebaseApp
import kotlinx.android.synthetic.main.activity_main.*
import org.koin.android.ext.android.inject

class MainActivity : AppCompatActivity() {

    private val userRepository: UserRepository by inject()

    private val navigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener {
        when (it.itemId) {
            R.id.nav_home -> viewPager.currentItem = 0
            R.id.nav_add_photo -> viewPager.currentItem = 1
            R.id.nav_friends -> viewPager.currentItem = 2
            R.id.nav_account -> viewPager.currentItem = 3
        }
        true
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        FirebaseApp.initializeApp(this)

        if (!userRepository.isLoggedIn()) {
            startActivity(Intent(this, LoginActivity::class.java))
            finish()
            return
        }

        val pagerAdapter = CustomPagerAdapter(supportFragmentManager)
        viewPager.adapter = pagerAdapter
        bottom_nav.setOnNavigationItemSelectedListener(navigationItemSelectedListener)
    }

    override fun onBackPressed() {
        if(viewPager.currentItem == 0) {
            super.onBackPressed()
        } else {
            bottom_nav.selectedItemId = R.id.nav_home
        }
    }

    override fun onResume() {
        if (!userRepository.isLoggedIn()) {
            startActivity(Intent(this, LoginActivity::class.java))
            finish()
            return
        }

        super.onResume()

    }
}