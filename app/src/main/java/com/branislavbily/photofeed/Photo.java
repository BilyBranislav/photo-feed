package com.branislavbily.photofeed;

import com.google.firebase.Timestamp;

import org.jetbrains.annotations.NotNull;

public class Photo {

    private String uri;
    private String poster;
    private String description;
    private Timestamp timestamp;

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    public String getPoster() {
        return poster;
    }

    public void setPoster(String poster) {
        this.poster = poster;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Timestamp getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Timestamp timestamp) {
        this.timestamp = timestamp;
    }

    public Photo(String uri, String poster, String description, Timestamp timestamp) {
        this.uri = uri;
        this.poster = poster;
        this.description = description;
        this.timestamp = timestamp;
    }

    public Photo() {}

    @NotNull
    @Override
    public String toString() {
        return "Photo{" +
                "uri='" + uri + '\'' +
                ", poster='" + poster + '\'' +
                ", description='" + description + '\'' +
                ", timestamp=" + timestamp +
                '}';
    }
}
