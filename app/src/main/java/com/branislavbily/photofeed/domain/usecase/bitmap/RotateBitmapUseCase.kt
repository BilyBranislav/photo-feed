package com.branislavbily.photofeed.domain.usecase.bitmap

import android.graphics.Bitmap
import android.graphics.Matrix
import android.net.Uri
import android.util.Log
import androidx.exifinterface.media.ExifInterface

interface RotateBitmapUseCase {
    fun rotate(bitmap: Bitmap, uri: Uri): Bitmap
}

class RotateBitmapUseCaseImpl(private val bitMapOrientationUseCase: OrientationBitMapUseCase)
    : RotateBitmapUseCase {

    override fun rotate(bitmap: Bitmap, uri: Uri): Bitmap {
        val matrix = Matrix()
        when (bitMapOrientationUseCase.get(uri)) {
            ExifInterface.ORIENTATION_FLIP_HORIZONTAL -> matrix.setScale(-1f, 1f)
            ExifInterface.ORIENTATION_ROTATE_180 -> matrix.setRotate(180f)
            ExifInterface.ORIENTATION_FLIP_VERTICAL -> {
                matrix.setRotate(180f)
                matrix.postScale(-1f, 1f)
            }
            ExifInterface.ORIENTATION_TRANSPOSE -> {
                matrix.setRotate(90f)
                matrix.postScale(-1f, 1f)
            }
            ExifInterface.ORIENTATION_ROTATE_90 -> matrix.setRotate(90f)
            ExifInterface.ORIENTATION_TRANSVERSE -> {
                matrix.setRotate(-90f)
                matrix.postScale(-1f, 1f)
            }
            ExifInterface.ORIENTATION_ROTATE_270 -> matrix.setRotate(-90f)
            else -> return bitmap
        }
        return try {
            Bitmap.createBitmap(bitmap, 0, 0, bitmap.width, bitmap.height, matrix, true)
        }
        catch (e:OutOfMemoryError) {
            Log.e(javaClass.name, "rotate: ", e)
            bitmap
        }
    }
}