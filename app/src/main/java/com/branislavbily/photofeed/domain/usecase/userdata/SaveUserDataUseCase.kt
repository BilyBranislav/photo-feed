package com.branislavbily.photofeed.domain.usecase.userdata

import com.branislavbily.photofeed.domain.repository.FirebaseIdsRepository
import com.branislavbily.photofeed.domain.repository.UserRepository
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.SetOptions

interface SaveUserDataUseCase {
    fun saveUserData(user: FirebaseUser)
}

class SaveUserDataUseCaseImpl(private val firebaseIdsRepository: FirebaseIdsRepository,
                              private val userRepository: UserRepository,
                              private val firestoreInstance: FirebaseFirestore): SaveUserDataUseCase {
    override fun saveUserData(user: FirebaseUser) {
        userRepository.getUID()?.let {
            firestoreInstance.collection(firebaseIdsRepository.getCollectionUsers())
                    .document(it)
                    .set(user, SetOptions.merge())
        }
    }
}