package com.branislavbily.photofeed.domain.usecase.bitmap.upload

import android.graphics.Bitmap
import java.io.ByteArrayOutputStream

interface CompressBitmapToByteArrayUseCase {
    fun compress(bitmap: Bitmap): ByteArray
}

class CompressBitmapToByteArrayUseCaseImpl: CompressBitmapToByteArrayUseCase {
    override fun compress(bitmap: Bitmap): ByteArray {
        val baos = ByteArrayOutputStream()
        bitmap.compress(Bitmap.CompressFormat.PNG, QUALITY, baos)
        return baos.toByteArray()
    }

    companion object {
        const val QUALITY = 75
    }
}