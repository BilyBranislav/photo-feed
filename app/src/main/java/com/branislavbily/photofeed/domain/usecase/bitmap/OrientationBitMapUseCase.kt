package com.branislavbily.photofeed.domain.usecase.bitmap

import android.content.ContentResolver
import androidx.exifinterface.media.ExifInterface
import android.net.Uri

interface OrientationBitMapUseCase {
    fun get(uri: Uri): Int
}

class OrientationBitMapUseCaseImpl(private val contentResolver: ContentResolver)
    : OrientationBitMapUseCase {

    override fun get(uri: Uri): Int {
        contentResolver.openInputStream(uri)?.let {
            return ExifInterface(it).getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_UNDEFINED)
        } ?: return ExifInterface.ORIENTATION_UNDEFINED
    }
}