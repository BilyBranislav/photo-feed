package com.branislavbily.photofeed.domain.usecase

import com.branislavbily.photofeed.Photo
import com.branislavbily.photofeed.domain.repository.FirebaseIdsRepository
import com.branislavbily.photofeed.domain.repository.UserRepository
import com.branislavbily.photofeed.domain.usecase.image.GetPhotoDataUserCase
import com.google.firebase.firestore.EventListener
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.Query

interface PhotoUriUseCase {
    fun getPhotos(eventListener: EventListener<ArrayList<Photo>?>)
}

class FirestorePhotoUriUseCase(private val userRepository: UserRepository,
                               private val firebaseRepository: FirebaseIdsRepository,
                               private val getPhotoDataUserCase: GetPhotoDataUserCase,
                               private val firestoreInstance: FirebaseFirestore)
    : PhotoUriUseCase {

    override fun getPhotos(eventListener: EventListener<ArrayList<Photo>?>) {
        var photos = ArrayList<Photo>()
        userRepository.getUID()?.let { userID ->
            firestoreInstance.collection(firebaseRepository.getCollectionUsers())
                    .document(userID)
                    .collection(firebaseRepository.getCollectionPhotos())
                    .orderBy(firebaseRepository.getFieldTimeStamp(), Query.Direction.DESCENDING)
                    .addSnapshotListener { queryDocumentSnapshots, _ ->
                        queryDocumentSnapshots?.let {
                            for (documentSnapshot in it) {
                                photos.add(getPhotoDataUserCase.getPhotoData(documentSnapshot))
                            }
                            eventListener.onEvent(photos, null)
                        }
                    }
        }
    }

}