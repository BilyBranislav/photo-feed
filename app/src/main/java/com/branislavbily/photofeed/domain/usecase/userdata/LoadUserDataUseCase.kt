package com.branislavbily.photofeed.domain.usecase.userdata

import com.branislavbily.photofeed.domain.repository.FirebaseIdsRepository
import com.branislavbily.photofeed.domain.repository.UserRepository
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.firestore.EventListener
import com.google.firebase.firestore.FirebaseFirestore

interface LoadUserDataUseCase {
    fun loadUserData(eventListener: EventListener<FirebaseUser>)
}

class LoadUserDataUseCaseImpl(private val userRepository: UserRepository,
                              private val firebaseIdsRepository: FirebaseIdsRepository,
                              private val firestoreInstance: FirebaseFirestore) : LoadUserDataUseCase {
    override fun loadUserData(eventListener: EventListener<FirebaseUser>) {
//        val firebaseUser = FirebaseUser()
//        userRepository.getUID()?.let {
//            firestoreInstance.collection(firebaseIdsRepository.getCollectionUsers())
//                    .document(it)
//                    .get()
//                    .addOnSuccessListener {
//                        firebaseUser.displayName = it.get("displayName")
//                    }
//
//        }
    }

}