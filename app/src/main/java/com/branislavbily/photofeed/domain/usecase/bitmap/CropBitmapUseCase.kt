package com.branislavbily.photofeed.domain.usecase.bitmap

import android.graphics.Bitmap

interface CropBitmapUseCase {
    fun crop(bitmap: Bitmap): Bitmap
}

class CropBitmapUseCaseImpl: CropBitmapUseCase {
    override fun crop(bitmap: Bitmap): Bitmap {
        val width = bitmap.width
        val height = bitmap.height
        val scale = if (width < height) width else height
        val x = (width - scale) / 2
        val y = (height - scale) / 2
        return Bitmap.createBitmap(bitmap, x, y, scale, scale)
    }
}