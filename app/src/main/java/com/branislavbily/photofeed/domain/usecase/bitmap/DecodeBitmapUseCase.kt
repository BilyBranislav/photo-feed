package com.branislavbily.photofeed.domain.usecase.bitmap

import android.content.ContentResolver
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri

interface DecodeBitmapUseCase {
    fun decode(uri: Uri): Bitmap
}

class DecodeBitmapUseCaseImpl(private val contentResolver: ContentResolver,
                              private val cropBitmapUseCase: CropBitmapUseCase,
                              private val rotateBitmapUseCase: RotateBitmapUseCase): DecodeBitmapUseCase {

    override fun decode(uri: Uri): Bitmap {
        var bitmap = BitmapFactory.decodeStream(contentResolver.openInputStream(uri))
        bitmap = cropBitmapUseCase.crop(bitmap)
        bitmap = rotateBitmapUseCase.rotate(bitmap, uri)
        return bitmap
    }

}

