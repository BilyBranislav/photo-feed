package com.branislavbily.photofeed.domain.repository

interface FirebaseIdsRepository {
    fun getCollectionUsers(): String
    fun getCollectionPhotos(): String
    fun getCollectionUserData(): String
    fun getFieldTimeStamp(): String
    fun getFieldUri(): String
    fun getFieldPoster(): String
    fun getFieldDescription(): String
    fun getProfilePhoto(): String
    fun getProfilePhotos(): String
    fun getDefaultProfilePicture(): String

}

class FirebaseIdsRepositoryImpl : FirebaseIdsRepository {
    override fun getDefaultProfilePicture(): String {
        return getProfilePhoto().plus("default.png")
    }

    override fun getProfilePhotos(): String {
        return "profilePhotos/"
    }

    override fun getProfilePhoto(): String {
        return "profilePhoto"
    }

    override fun getFieldTimeStamp(): String {
        return "timestamp"
    }

    override fun getFieldUri(): String {
        return "uri"
    }

    override fun getFieldPoster(): String {
        return "poster"
    }

    override fun getFieldDescription(): String {
        return "description"
    }

    override fun getCollectionUserData(): String {
        return "user_data"
    }

    override fun getCollectionUsers(): String {
        return "users"
    }

    override fun getCollectionPhotos(): String {
        return "photos"
    }
}