package com.branislavbily.photofeed.domain.usecase.image

import com.branislavbily.photofeed.Photo
import com.branislavbily.photofeed.domain.repository.FirebaseIdsRepository
import com.google.firebase.firestore.DocumentSnapshot

interface GetPhotoDataUserCase {
    fun getPhotoData(documentSnapshot: DocumentSnapshot): Photo
}

class GetPhotoDataUserCaseImpl(
        private val firebaseIdsRepository: FirebaseIdsRepository)
    : GetPhotoDataUserCase {

    override fun getPhotoData(documentSnapshot: DocumentSnapshot): Photo {
        val photo = Photo()
        documentSnapshot.getString(firebaseIdsRepository.getFieldUri())?.let { uri ->
            photo.uri = uri
        }
        documentSnapshot.getTimestamp(firebaseIdsRepository.getFieldTimeStamp())?.let { timestamp ->
            photo.timestamp = timestamp
        }
        documentSnapshot.getString(firebaseIdsRepository.getFieldPoster())?.let { poster ->
            photo.poster = poster
        }
        documentSnapshot.getString(firebaseIdsRepository.getFieldDescription())?.let { description ->
            photo.description = description
        }
        return photo
    }

}