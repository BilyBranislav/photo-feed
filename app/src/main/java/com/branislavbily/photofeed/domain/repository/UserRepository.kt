package com.branislavbily.photofeed.domain.repository

import android.content.Context
import android.content.SharedPreferences

interface UserRepository {
    fun setUID(uid: String)
    fun getUID(): String?
    fun isLoggedIn(): Boolean
    fun logOut()
}

class SharedPreferencesUserRepository(private val context: Context) : UserRepository {

    private val sharedPreferences: SharedPreferences by lazy {
        context.getSharedPreferences(KEY_PREFERENCES_FILE, 0)
    }

    override fun setUID(uid: String) {
        sharedPreferences.edit()
                .putString(KEY_USER, uid)
                .apply()
    }

    override fun getUID(): String? {
        return sharedPreferences.getString(KEY_USER, "")
    }

    override fun isLoggedIn(): Boolean {
        return sharedPreferences.getString(KEY_USER, "")?.isEmpty() == false

    }

    override fun logOut() {
        sharedPreferences.edit()
                .putString(KEY_USER, "")
                .apply()
    }

    companion object {
        const val KEY_USER = "user_uid"
        const val KEY_PREFERENCES_FILE = "preferences"
    }
}
