package com.branislavbily.photofeed.adapters

import android.databinding.BindingAdapter
import android.graphics.Bitmap
import android.widget.ImageView

@BindingAdapter("android:src")
fun setBitmap(view: ImageView, bitmap: Bitmap?) {
     bitmap?.let { view.setImageBitmap(it) }
}