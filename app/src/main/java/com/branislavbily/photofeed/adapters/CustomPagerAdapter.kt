package com.branislavbily.photofeed.adapters

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import com.branislavbily.photofeed.fragments.AccountFragment
import com.branislavbily.photofeed.fragments.AddImageFragment
import com.branislavbily.photofeed.fragments.FriendsFragment
import com.branislavbily.photofeed.fragments.HomeFragment

class CustomPagerAdapter(fm: FragmentManager) : FragmentPagerAdapter(fm) {
    override fun getItem(p0: Int): Fragment {
        return when(p0) {
            0 -> HomeFragment()
            1 -> AddImageFragment()
            2 -> FriendsFragment()
            else -> AccountFragment()
        }
    }

    override fun getCount(): Int {
        return 4
    }
}