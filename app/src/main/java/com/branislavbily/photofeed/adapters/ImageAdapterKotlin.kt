package com.branislavbily.photofeed.adapters

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.branislavbily.photofeed.Photo
import com.branislavbily.photofeed.R
import com.branislavbily.photofeed.glide.GlideApp
import com.google.firebase.storage.FirebaseStorage
import kotlinx.android.synthetic.main.image_item.view.*

class ImageAdapterKotlin : RecyclerView.Adapter<ImageAdapterKotlin.ImageHolder>() {

    private var photos = ArrayList<Photo>()

    override fun onCreateViewHolder(viewGroup: ViewGroup, p1: Int): ImageHolder {
        val view = LayoutInflater.from(viewGroup.context).inflate(R.layout.image_item, viewGroup, false)
        return ImageHolder(view)
    }

    override fun getItemCount(): Int {
        return photos.size
    }

    override fun onBindViewHolder(imageHolder: ImageHolder, position: Int) {
        imageHolder.bind(photos[position])
    }

    fun setNewData(newData: ArrayList<Photo>?) {
        newData?.let {
            photos = newData
        }
        notifyDataSetChanged()
    }

    class ImageHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind(photo: Photo) {
            itemView.textViewPosterName.text = photo.poster
            itemView.textViewDescription.text = photo.description
            val storageReference = FirebaseStorage.getInstance().getReference(photo.uri)
            GlideApp.with(itemView)
                    .load(storageReference)
                    .placeholder(R.color.grey)
                    .into(itemView.imageViewItem)
        }
    }
}