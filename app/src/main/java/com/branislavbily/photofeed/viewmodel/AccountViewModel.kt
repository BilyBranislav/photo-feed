package com.branislavbily.photofeed.viewmodel

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import com.google.firebase.auth.FirebaseUser

class AccountViewModel(firebaseUser: FirebaseUser?): ViewModel() {
    val userNameLd = MutableLiveData<String>().also {
        it.postValue(firebaseUser?.displayName)
    }
    val emailLd = MutableLiveData<String>().also {
        it.postValue(firebaseUser?.email)
    }

    val logout = MutableLiveData<Boolean>()

    fun logout() {
        logout.postValue(true)
    }
}