package com.branislavbily.photofeed.viewmodel

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import android.graphics.Bitmap
import android.net.Uri
import com.branislavbily.photofeed.Photo
import com.branislavbily.photofeed.domain.repository.FirebaseIdsRepository
import com.branislavbily.photofeed.domain.repository.UserRepository
import com.branislavbily.photofeed.domain.usecase.bitmap.DecodeBitmapUseCase
import com.branislavbily.photofeed.domain.usecase.bitmap.upload.CompressBitmapToByteArrayUseCase
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.storage.FirebaseStorage
import java.util.*

class AddImageViewModel(private val decodeBitmapUseCase: DecodeBitmapUseCase,
                        private val compressBitmapToByteArrayUseCase: CompressBitmapToByteArrayUseCase,
                        private val userRepository: UserRepository,
                        private val firebaseRepository: FirebaseIdsRepository,
                        private val firebaseFirestore: FirebaseFirestore,
                        private val firebaseStorage: FirebaseStorage)
    : ViewModel() {

    val startImagePickLd = MutableLiveData<Void>()
    val loadingInProgressLd = MutableLiveData<Boolean>()
    val toastLd = MutableLiveData<String>()

    val bitmapLd = MutableLiveData<Bitmap?>()

    fun uploadImage() {
        bitmapLd.value?.let {
            loadingInProgressLd.postValue(true)
            val data = compressBitmapToByteArrayUseCase.compress(it)

            val path = "photos/" + UUID.randomUUID() + ".png"
            val screenshotsRef = firebaseStorage.getReference(path)

            val uploadTask = screenshotsRef.putBytes(data)
            uploadTask.addOnSuccessListener { taskSnapshot ->
                val uri = taskSnapshot.metadata?.path
                userRepository.getUID()?.let { userId ->
                    firebaseFirestore.collection(firebaseRepository.getCollectionUsers())
                            .document(userId)
                            .collection(firebaseRepository.getCollectionPhotos())
                            .add(Photo(uri,   FirebaseAuth.getInstance().currentUser?.displayName, "Description", com.google.firebase.Timestamp.now()))
                    toastLd.postValue("Successfully uploaded")
                }
            }

            uploadTask.addOnFailureListener {
                toastLd.postValue("An error occurred while uploading")
            }

            uploadTask.addOnCompleteListener {
                loadingInProgressLd.postValue(false)
                bitmapLd.postValue(Bitmap.createBitmap(400, 400, Bitmap.Config.ARGB_8888))
            }
        } ?: run {
             toastLd.postValue("Upload image first!")
        }
    }

    fun onAddClicked() {
        startImagePickLd.postValue(null)
    }

    fun onImageSelected(uri: Uri?) {
        uri?.let { bitmapLd.postValue(decodeBitmapUseCase.decode(it)) }
                ?: toastLd.postValue("An error occurred")
    }

}