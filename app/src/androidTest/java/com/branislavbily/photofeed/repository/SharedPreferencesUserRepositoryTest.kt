package com.branislavbily.photofeed.repository

import android.support.test.InstrumentationRegistry
import com.branislavbily.photofeed.domain.repository.SharedPreferencesUserRepository
import com.iamhabib.easy_preference.EasyPreference
import org.junit.After
import org.junit.Assert.assertEquals
import org.junit.Test

class SharedPreferencesUserRepositoryTest {

    private val userRepository = SharedPreferencesUserRepository(InstrumentationRegistry.getTargetContext())

    @Test
    fun setUIDShouldSucceed() {
        var username = "TEST TEST 1"
        userRepository.setUID(username)
        assertEquals(username, userRepository.getUID())

        username = ""
        userRepository.setUID(username)
        assertEquals(username, userRepository.getUID())
    }

    @Test
    fun isLoggedIn() {
        userRepository.setUID("A")
        assertEquals(true, userRepository.isLoggedIn())

        userRepository.setUID("")
        assertEquals(false, userRepository.isLoggedIn())
    }

    @Test
    fun logOut() {
        var username = "TEST TEST 1"
        userRepository.setUID(username)
        userRepository.logOut()
        assertEquals("", userRepository.getUID())
        assertEquals(false, userRepository.isLoggedIn())

        username = ""
        userRepository.setUID(username)
        userRepository.logOut()
        assertEquals("", userRepository.getUID())
        assertEquals(false, userRepository.isLoggedIn())
    }

    @After
    fun tearDown() {
        EasyPreference.with(InstrumentationRegistry.getTargetContext())
                .clearAll()
                .save()
    }
}