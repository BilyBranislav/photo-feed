package com.branislavbily.photofeed.usecase

import android.content.res.Resources
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Matrix
import android.media.ExifInterface
import android.net.Uri
import com.branislavbily.photofeed.R
import com.branislavbily.photofeed.domain.usecase.bitmap.OrientationBitMapUseCase
import com.branislavbily.photofeed.domain.usecase.bitmap.RotateBitmapUseCaseImpl
import org.junit.Assert.assertEquals
import org.junit.Assert.assertNotEquals
import org.junit.Test
import org.mockito.Mockito.`when`
import org.mockito.Mockito.mock

class RotateBitmapUseCaseImplTest {

    @Test
    fun rotateUndefined() {
        val mockedOrientation = mock(OrientationBitMapUseCase::class.java)
        `when`(mockedOrientation.get(Uri.EMPTY)).then {
            ExifInterface.ORIENTATION_UNDEFINED
        }

        val rotateBitmapUseCase = RotateBitmapUseCaseImpl(mockedOrientation)
        val bitmap = BitmapFactory.decodeResource(Resources.getSystem(), R.drawable.no_image)
        bitmap?.let {
            var rotateBitmap = rotateBitmapUseCase.rotate(bitmap, Uri.EMPTY)
            assertEquals(bitmap, rotateBitmap)

            val matrix = Matrix()
            matrix.setRotate(180f)
            matrix.postScale(-1f, 1f)
            rotateBitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.width, bitmap.height, matrix, true)
            assertNotEquals(rotateBitmap, bitmap)
        }
    }

    @Test
    fun rotateHorizontal() {
        val mockedOrientation = mock(OrientationBitMapUseCase::class.java)
        `when`(mockedOrientation.get(Uri.EMPTY)).then {
            ExifInterface.ORIENTATION_FLIP_HORIZONTAL
        }

        val rotateBitmapUseCase = RotateBitmapUseCaseImpl(mockedOrientation)
        val bitmap = BitmapFactory.decodeResource(Resources.getSystem(), R.drawable.no_image)
        bitmap?.let {
            var rotateBitmap = rotateBitmapUseCase.rotate(bitmap, Uri.EMPTY)
            assertNotEquals(bitmap, rotateBitmap)
            val matrix = Matrix()
            matrix.setScale(-1f, 1f)
            rotateBitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.width, bitmap.height, matrix, true)
            assertEquals(rotateBitmap, bitmap)
        }
    }

    @Test
    fun rotateVertical() {
        val mockedOrientation = mock(OrientationBitMapUseCase::class.java)
        `when`(mockedOrientation.get(Uri.EMPTY)).then {
            ExifInterface.ORIENTATION_FLIP_VERTICAL
        }

        val rotateBitmapUseCase = RotateBitmapUseCaseImpl(mockedOrientation)
        val bitmap = BitmapFactory.decodeResource(Resources.getSystem(), R.drawable.no_image)
        bitmap?.let {
            var rotateBitmap = rotateBitmapUseCase.rotate(bitmap, Uri.EMPTY)
            assertNotEquals(bitmap, rotateBitmap)
            val matrix = Matrix()
            matrix.setRotate(180f)
            matrix.postScale(-1f, 1f)
            rotateBitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.width, bitmap.height, matrix, true)
            assertEquals(rotateBitmap, bitmap)
        }
    }

    @Test
    fun rotateTransverse() {
        val mockedOrientation = mock(OrientationBitMapUseCase::class.java)
        `when`(mockedOrientation.get(Uri.EMPTY)).then {
            ExifInterface.ORIENTATION_TRANSVERSE
        }

        val rotateBitmapUseCase = RotateBitmapUseCaseImpl(mockedOrientation)
        val bitmap = BitmapFactory.decodeResource(Resources.getSystem(), R.drawable.no_image)
        bitmap?.let {
            var rotateBitmap = rotateBitmapUseCase.rotate(bitmap, Uri.EMPTY)
            assertNotEquals(bitmap, rotateBitmap)
            val matrix = Matrix()
            matrix.setRotate(-90f)
            matrix.postScale(-1f, 1f)
            rotateBitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.width, bitmap.height, matrix, true)
            assertEquals(rotateBitmap, bitmap)
        }
    }

    @Test
    fun rotateTranspose() {
        val mockedOrientation = mock(OrientationBitMapUseCase::class.java)
        `when`(mockedOrientation.get(Uri.EMPTY)).then {
            ExifInterface.ORIENTATION_TRANSVERSE
        }

        val rotateBitmapUseCase = RotateBitmapUseCaseImpl(mockedOrientation)
        val bitmap = BitmapFactory.decodeResource(Resources.getSystem(), R.drawable.no_image)
        bitmap?.let {
            var rotateBitmap = rotateBitmapUseCase.rotate(bitmap, Uri.EMPTY)
            assertNotEquals(bitmap, rotateBitmap)
            val matrix = Matrix()
            matrix.setRotate(90f)
            matrix.postScale(-1f, 1f)
            rotateBitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.width, bitmap.height, matrix, true)
            assertEquals(rotateBitmap, bitmap)
        }
    }

    @Test
    fun rotate180() {
        val mockedOrientation = mock(OrientationBitMapUseCase::class.java)
        `when`(mockedOrientation.get(Uri.EMPTY)).then {
            ExifInterface.ORIENTATION_ROTATE_180
        }

        val rotateBitmapUseCase = RotateBitmapUseCaseImpl(mockedOrientation)
        val bitmap = BitmapFactory.decodeResource(Resources.getSystem(), R.drawable.no_image)
        bitmap?.let {
            var rotateBitmap = rotateBitmapUseCase.rotate(bitmap, Uri.EMPTY)
            assertNotEquals(bitmap, rotateBitmap)
            val matrix = Matrix()
            matrix.setRotate(180f)
            rotateBitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.width, bitmap.height, matrix, true)
            assertEquals(rotateBitmap, bitmap)
        }
    }

    @Test
    fun rotate270() {
        val mockedOrientation = mock(OrientationBitMapUseCase::class.java)
        `when`(mockedOrientation.get(Uri.EMPTY)).then {
            ExifInterface.ORIENTATION_ROTATE_270
        }

        val rotateBitmapUseCase = RotateBitmapUseCaseImpl(mockedOrientation)
        val bitmap = BitmapFactory.decodeResource(Resources.getSystem(), R.drawable.no_image)
        bitmap?.let {
            var rotateBitmap = rotateBitmapUseCase.rotate(bitmap, Uri.EMPTY)
            assertNotEquals(bitmap, rotateBitmap)
            val matrix = Matrix()
            matrix.setRotate(-90f)
            rotateBitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.width, bitmap.height, matrix, true)
            assertEquals(rotateBitmap, bitmap)
        }
    }

    @Test
    fun rotate90() {
        val mockedOrientation = mock(OrientationBitMapUseCase::class.java)
        `when`(mockedOrientation.get(Uri.EMPTY)).then {
            ExifInterface.ORIENTATION_ROTATE_90
        }

        val rotateBitmapUseCase = RotateBitmapUseCaseImpl(mockedOrientation)
        val bitmap = BitmapFactory.decodeResource(Resources.getSystem(), R.drawable.no_image)
        bitmap?.let {
            var rotateBitmap = rotateBitmapUseCase.rotate(bitmap, Uri.EMPTY)
            assertNotEquals(bitmap, rotateBitmap)
            val matrix = Matrix()
            matrix.setRotate(90f)
            rotateBitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.width, bitmap.height, matrix, true)
            assertEquals(rotateBitmap, bitmap)
        }
    }






}